create table agenda_paginacion(
    id serial not null,
    nombre varchar,
    nickname varchar,
    telefono int,
    primary key(id)
);

alter table agenda_paginacion
add constraint UQ_nickname
unique (nickname);

insert into agenda_paginacion(nombre, nickname,telefono) values
('Carlos','carloncho',2345667),
('Camila','camichan',23456889),
('José','pepote',23744343),
('Julieta','chicasersi',2148890),
('Raul','raulote',2345667),
('Victor','vitoco',2458521),
('José','tito',999999),
('Clarita','annietto',90909090),
('Cesar','queso',344323234),
('Natalia','natty',123234324),
('Támara','tammy',423432),
('Isabel','icha',121323231),
('Tábata','taby23',545445445),
('Julián','poseidon_sama',67655432),
('Francisco','panchoso',423324),
('Eva','chicaeva',5435354);